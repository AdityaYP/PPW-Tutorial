Catatan Checklist:

Chat Box:
- Menambahkan lab_6.html pada folder templates
- Menambahkan lab_6.css pada folder ./static/css
- Menambahkan lab_6.js pada folder ./static/js
- Melengkapi potongan kode pada lab_6.js agar dapat berjalan
	- Menambahkan method hide dan show, untuk show/hide chat box
	- Menggunakan event.preventDefault untuk menghandle agar kembali seperti semula, tidak ter-enter ke bawah ketika memasukkan chat
	- Langsung memanggil method hide() di (documents).ready(function) agar chatBox tertutup terlebih dahulu saat membuka web

Kalkulator:
- Menambahkan potongan kode ke dalam lab_6.html pada folder templates
- Menambahkan potongan kode ke dalam file lab_6.css pada folder ./static/css
- Menambahkan potongan kode ke lab_6.js
- Implementasi fungsi ac, dengan menambahkan kode
	if(x === 'ac'){
		print.value = ""
	}
- Menambahkan operasi Log, Sin, Tan, menggunakan method dari Math seperti Math.sin(x), Math.log(x), Math.tan(x)

Fitur select themes
- Load theme default sesuai selectedTheme
- Populate data themes dari local storage ke select2
- Local storage berisi themes dan selectedTheme
- Warna berubah ketika theme dipilih
- Menggunakan localStorage.selectedTheme dengan baik agar ketika reload tampilan theme tetap pada theme yang baru diganti
- Menggunakan for loop untuk menambahkan tag Option di select, agar otomatis membuat list themes berdasarkan localStorage theme yang telah di definisikan
